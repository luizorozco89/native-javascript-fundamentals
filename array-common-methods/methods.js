// Data resources
const companies= [
    {name: "Company One", category: "Finance", start: 1981, end: 2004},
    {name: "Company Two", category: "Retail", start: 1992, end: 2008},
    {name: "Company Three", category: "Auto", start: 1999, end: 2007},
    {name: "Company Four", category: "Retail", start: 1989, end: 2010},
    {name: "Company Five", category: "Technology", start: 2009, end: 2014},
    {name: "Company Six", category: "Finance", start: 1987, end: 2010},
    {name: "Company Seven", category: "Auto", start: 1986, end: 1996},
    {name: "Company Eight", category: "Technology", start: 2011, end: 2016},
    {name: "Company Nine", category: "Retail", start: 1981, end: 1989}
  ];

const ages = [33, 12, 20, 16, 5, 54, 21, 44, 61, 13, 15, 45, 25, 64, 32];

// foreach method - printing items on console
companies.forEach(item => console.log(item));

// map method - printing items on console
companies.map(item => console.log(item));

// Filter method: Print items with category = "Auto";
var autoCat = companies.filter(item => item.category === 'Auto');
console.log(autoCat);

// Combining filter and forEach methods: Print items with category = Retail
companies
    .filter(item => item.category === 'Retail')
    .forEach(item => console.log(item));

// Combining filter and map methods: Print items with category = Finance
companies
    .filter(item => item.category === 'Finance')
    .map(item => console.log(item));

// Print companies founded after 1986
companies
    .filter(item => item.start > 1986)
    .forEach(item => console.log(item));

// Print ages equal or bigger than 18
ages
    .filter(age => age >= 18)
    .forEach(age => console.log(age));

// Filtering companies that started in the 90's, then sorting them by year, and then printing them
companies
    .filter(company => company.start >= 1990)
    .sort((c1,c2) => c1.start > c2.start ? 1 : -1)
    .map(company => console.log(company));

// Using sort without argumrments, it doesn't work promerly
// to fix it, we must pass arguments
ages
    .sort()
    .map(age => console.log(age));    

// Using sort with arguments, elements will be sorted in ascending order
ages
  .sort((a,b) => a - b)
  .map(age => console.log(age));

// Using sort with arguments, elements will be sorted in descending order
ages
  .sort((a,b) => b - a)
  .map(age => console.log(age));

// Using for loop to sum all the ages
var sumFor = 0;

for(var i = 0; i < ages.length; i++) {
    sumFor += ages[i];
}

console.log(sumFor);

// Using reduce to sum all the ages
var sumReduce = ages.reduce((total,age) => total + age, 0);

console.log('Sum with reduce => ', sumReduce);
