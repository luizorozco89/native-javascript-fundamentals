
// Exercise method with Object.create()

const obj1 = {
    prop1: 'Prop one',
    method: function() {
        console.log(this.prop1);
    }
}

const obj2 = Object.create(obj1);

console.log('obj2 prop1 property:');
console.log(obj2.prop1);
console.log('obj2 executes method:');
console.log(obj2.method());

obj2.getPropOne = function() {
    console.log(this.prop1);
}

console.log('Accessing prop1 property by prototype chain');
obj2.getPropOne();

console.log('obj1:');
console.log(obj1);
console.log('obj2:');
console.log(obj2);

