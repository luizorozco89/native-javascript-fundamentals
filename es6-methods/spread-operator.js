// Exercise with spread operator

const myArray = [1,2,3,4];

// Copying the content of 'myArray' into a new array named 'myArrayCopy', this does not mutate
// the original array, it's a way to create a copy with inmutability, another way is using .slice() method
const myArrayCopy = [...myArray];

// Logging the original array
console.log('Original array => ', myArray);

// Logging the new array
console.log('Copy of the original array => ', myArrayCopy);

// Checking if the arrays points to different memory spaces, they do
// this means the arrays are different and independent from each other
console.log(myArray === myArrayCopy);

// Mutating original array
myArray.pop();
console.log('Original array mutated => ', myArray);
console.log('Copy of the original array => ', myArrayCopy);

// Using spread operator with objects
const obj = {
    a: 'Prop a in obj',
    b: 'Prop b in obj'
};

let obj2 = {...obj};

console.log('Original object => ', obj);
console.log('Copy of the original object => ', obj);

// Checking if obj and obj2 points to different spaces in memory, they do
console.log(obj === obj2);

// Changing and replaing the value of prop 1 into obj2
obj2 = {...obj2, a: 'Prop a changed in obj2'}

// obj2 has now a different value for prop a
console.log('obj 2 => ', obj2);

// Another example of the use of spread operator
let x = [1,2,3];

const myF = function(...arg) {
    console.log(...arg);
}

myF(...x);