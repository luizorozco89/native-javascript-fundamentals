
// Exercise with rest operator
// Rest operator allows to compress several arguments into an array

const display = (...rest) => console.log(...rest);

display(1,2,3,4,5,6,7);

const multiplier = (mult, ...rest) => {
    let args = [...rest];
    args.map(el => console.log(mult * el));
};

multiplier(3,1,2,3,4,5);

