// Destructuring exercise
// Object with several properties
var person = {
    name: 'Luis',
    age: 29,
    favHobby: 'Plays soccer'
}

// Properties 'name' and 'age' stored into individual variables using destructuring
var { name, age } = person;

// Logging individual variables created by destructuring 'sayan' object
console.log('Name => ', name);
console.log('Age => ', age);

// Function that logs persons info, it expects an object and it destructs it into three variables
// for variable 'favHobby' set a default value of 'has no hobby' in case there is
// no value received as argument

const displayInfo = ({name, age, favHobby='has no hobby'}) => 
    console.log(`${name} is ${age} years old and he ${favHobby}`);

// Executing function passing an object as argument
displayInfo({name: 'Michael', age: 30});
