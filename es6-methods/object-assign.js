
// Exercise with Object.assign() method

const obj1 = {
    a: 'a value in obj1',
    b: 'b value in obj1'
};

const obj2 = {
    c: 'c value in obj2',
    d: 'd value in obj2'
};

// Merging two objects into an empty one, returning a new object with all the properties
// of the two objectes merged, this methods doesn't mutate the original objects
const objsMerged = Object.assign({},obj1,obj2);

console.log('obj1 => ', obj1);
console.log('obj2 => ', obj2);
console.log('Objects merged:');
console.log(objsMerged);

// Trying merging two objects without passing an empty object as the first argument
Object.assign(obj1,obj2);

// Checking if the method mutates or not the original objects as there was not an empty
// object passed to receive all the properties, it is indeed mutated, but only the first object
// passed as the first argument
console.log('obj1 => ', obj1);
console.log('obj2 => ', obj2);

// Trying to merge two objects with same properties names
// properties in obj4 overwrite the properties in obj3
//
const obj3 = {
    a: 'a in obj3',
    b: 'b in obj3'
};

const obj4 = {
    a: 'a in obj4',
    b: 'b in obj 4'
}

console.log('obj3 and obj4 merged:');
console.log(Object.assign({},obj3,obj4));
console.log('obj 3 => ', obj3);
console.log('obj 4 => ', obj4);