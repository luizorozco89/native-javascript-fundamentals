// Implementation of the functionality of new keyword behind the scenes

/* 
new keyword in javascript works in the following way to create an instance of a 
constructor function:

1. First, it creates a new object.
2. Then it is going to look at whatever we called new on(constructor function), and it is going to check that function’s prototype object, and it is going to set the prototype of the new object that has just created to be that object.
3. Then, it is going to look again at whatever we called new on(constructor function), and it is going to call that function, but it is going to call it with the new object that was created in the first step, assigned to the this variable.
4. And the final step new does is to return the new created object.
*/

function Person(name, age) {
    this.name = name;
    this.age = age;
}

Person.prototype.talk = function() {
    console.log(this.name,'is talking...');
}

function myNew(fun) {
    let obj = {};
    Object.setPrototypeOf(obj,fun.prototype);
    let myArray = Array.prototype.slice.apply(arguments);
    fun.apply(obj,myArray.slice(1));
    return obj;
}

let luis = myNew(Person,'Luis',29);

console.log('Instance created => ', luis);
console.log('Method added to conbstructor prototype object');
console.log(luis.talk());