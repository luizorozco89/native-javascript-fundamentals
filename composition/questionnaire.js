// Questionnaire manager created using composition
//Global id
let id = 1;

//Ids generator
const getNewId = () => id++;

//Question creator
const createQuestion = (questions) => ({
    addQuestion: (question, questionType, correctAnswer, options) => {
        if(options) {
            questions.push({
                id: getNewId(),
                question,
                questionType,
                correctAnswer,
                options,
                userAnswer: null
            });
        }else {
            questions.push({
                id: getNewId(),
                question,
                questionType,
                correctAnswer,
                userAnswer: null
            });
        }
    }
});

/*const tempQuestions = [
    {id: 1, question: 'Q1', questionType: 'Multiple', correctAnswer: 'a', options: {a: 'A1', b: 'Option two', c: 'Option three'}, userAnswer: 'a'},
    {id: 2, question: 'Q2', questionType: 'Yes/No', correctAnswer: 'Yes', userAnswer: 'yes'},
    {id: 3, question: 'Q3', questionType: 'Open', correctAnswer: 'Open Answer', userAnswer: 'open answer'},
];*/

//Questionnaire evaluator
const qEvaluator = questions => ({
    evaluate: () => {
        let count = 0;
        questions.map(question => {
            const correctAnswer = question.correctAnswer.toUpperCase();
            const userAnswer = question.userAnswer.toUpperCase();
            correctAnswer === userAnswer ? count++ : null; 
        });
        let per = (count * 100) / questions.length;
        return {rate: per.toFixed(2), correctAnswers: count, result: per >= 80 ? 'Approved' : 'Failed' }
        //console.log(`User's correct answers: ${count}, rate: ${per}%, ${per >= 80 ? 'Approved' : 'Failed'}`);
    }
});

/*const result = qEvaluator(tempQuestions).evaluate();
console.log(result);*/

//Question answerer
const questionAnswerer = questions => ({
    answerQuestion: (id,answer) => {
        questions.map(question => {
            question.id === id ? question.userAnswer = answer : null;
        });
    }
});

//questionAnswerer(tempQuestions).answerQuestion(1,'a');

//Edit question
const questionEditer = questions => ({
    editQuestion: (id, updatedInfo) => {
        let question = questions.filter(question => question.id === id);
        let index = questions.indexOf(question[0]);
        questions[index] = { ...questions[index], ...updatedInfo };
    }
});

//questionEditer(tempQuestions).editQuestion(5,{question: 'Q1 updated', correctAnswer: 'A1 updated'});

//Delete question
const questionDeleter = questions => ({
    deleteQuestion: (id) => {
        let index = null;
        questions.map(question => {
            question.id === id ? index = questions.indexOf(question) : null;
        });
        questions.splice(index,1);
    }
});

//Question printer
const questionsPrinter = questions => ({
    printQuestions: () => {
        questions.map(question => console.log(question));
    }
});

//Creation of a Questionnaire
function Questionnaire(name,subject) {
    const state = {
        name,
        subject,
        questions: []
    }
    return Object.assign(
        {}, state, createQuestion(state.questions), 
        questionEditer(state.questions), 
        questionDeleter(state.questions), 
        questionsPrinter(state.questions),
        questionAnswerer(state.questions),
        qEvaluator(state.questions));
}

const myQ = Questionnaire('My questionnaire','Math');
console.log('Questionnaire created:', myQ);
myQ.addQuestion('Is 2 + 2 = 4?','Yes/No','Yes');
myQ.addQuestion('Is 2 * 3 = 6?','Yes/No','Yes');
myQ.addQuestion('What is Newton first name?','Open','Issac');
myQ.addQuestion('How much is 8 / 4?','Multiple','a',{ a: '2', b: '4', c: '7', d: '10' });
myQ.addQuestion('Why do you like Math?','Open','Because it is great!!');
myQ.addQuestion('Is 2 * 10 = 30?','Yes/No','No');
console.log('Five questions added:');
myQ.printQuestions();
console.log('Answering Math questionnaire...');
myQ.answerQuestion(1,'Yes');
myQ.answerQuestion(2,'Yes');
myQ.answerQuestion(3,'Issac');
myQ.answerQuestion(4,'a');
myQ.answerQuestion(5,'I do not like it!!');
myQ.answerQuestion(6,'No');
const result = myQ.evaluate();
console.log('Evaluation result:');
console.log(result);