// Recursion functionality to create trees of categories and sub-categories

// Data resources
const categories = [
    {id: 'vehicles', parent: null},
    {id: 'cars', parent: 'vehicles'},
    {id: 'sedans', parent: 'cars'},
    {id: 'coupes', parent: 'cars'},
    {id: 'honda-accord', parent: 'sedans'},
    {id: 'mazda-3', parent: 'sedans'},
    {id: 'peugeot-407', parent: 'coupes'},
    {id: 'bmw-4', parent: 'coupes'}
];

// Recursive function
let makeTree = (categories,parent) => {
    let obj = {};
    categories
        .filter(category => category.parent === parent)
        .forEach(element => {
            obj[element.id] = makeTree(categories,element.id);
        });
    return obj;
};

let myResult = makeTree(categories,null);
console.log(myResult);

//Expected result
/*let result = {
    vehicles: {
        cars: {
            coupes: {
                bmw-4: {},
                peugeot-407: {}
            },
            sedans: {
                honda-accord: {},
                mazda-3: {}
            }
        }
    }
};*/